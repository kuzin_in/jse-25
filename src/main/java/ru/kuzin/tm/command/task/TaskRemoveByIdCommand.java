package ru.kuzin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().removeById(userId, id);
    }

}