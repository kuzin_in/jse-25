package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}